//----------------------------------------------------------------------------
// Fichier "MainAlphabet.cpp"
// Application console.
// Application qui affiche l'alphabet. Affichage dans
// une console d'exécution.
// JBD le 19/01/2021
//----------------------------------------------------------------------------
#include <iostream> // Pour cout, cin et endl.
using namespace std; // Pour enlever std::.
int main() {
    char NB_CAR0 = 'A' ; // Variable de nos caractères pour la boucle 1.
    char NB_CAR1 = 'A' ; // Variable de nos caractères pour la boucle 2.

    cout << "Affichage des lettres de l'alphabet en majuscules." << endl << "Solution 1 (Avec tant que) : " << endl ;
    cout << " " ;
    // Solution 1 avec une boucle tant que NB_CAR0 < z.
    while (NB_CAR0 <= 'Z') {
        cout << NB_CAR0 << " " ;
        ++NB_CAR0 ;
    }

    cout << endl << "Solution 2 (Avec répéter...jusqu'à) : " << endl ;
    cout << " " ;

    // Solution 2 avec une boucle répéter jusqu'à NB_CAR1 < z.
    do {
        cout << NB_CAR1 << " ";
        ++NB_CAR1 ;
    } while (NB_CAR1 <= 'Z') ;

    cout << endl << "Solution 3 (Avec pour) : " << endl ;
    cout << " " ;

    // Solution 2 avec une boucle pour NB_CAR2 = z jusqu'à NB_CAR2 < z avec un pas de 1.
    for (char NB_CAR2 = 'A'; NB_CAR2 <= 'Z'; ++NB_CAR2) {
        cout << NB_CAR2 << " " ;
    }
    cout << endl << "Fin de programme." ;
    return 0;
}