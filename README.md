# Premier projet SNIR1 🗂

> * Auteur : Jean-Baptiste D
> * Date : 28/01/2021
> * Client : Manjaro GNOME 20.2.1

Création du premier projet Gitlab SNIR 1.  

* Création d'un projet C++.
* Création de la clé ssh.
* Finalisation avec un push.

![git](img/git.jpg)

*:D*
